FROM node:alpine

ENV S6_KEEP_ENV=1

RUN set -x && \
    apk add --no-cache \
    libgcc \
    libstdc++ \
    curl \
    curl-dev \
    coreutils \
    tzdata \
    shadow \
    libstdc++ \
    paxctl \
    logrotate \
    bash

# Install s6 overlay
RUN wget https://github.com/just-containers/s6-overlay/releases/download/v1.21.4.0/s6-overlay-amd64.tar.gz -O s6-overlay.tar.gz && \
    tar xfv s6-overlay.tar.gz -C / && \
    rm -r s6-overlay.tar.gz

# Add user
RUN addgroup -g 911 abc && \
    adduser -u 911 -D -G abc abc && \
    groupmod -og 998 ping && \
    addgroup docker -g 999 && \
    usermod -aG docker abc

# Make some folders
RUN mkdir -p /app /config /defaults /data

# Cleanup
RUN apk del --purge \
  && rm -rf /tmp/* \
  && sed -i "s#/var/log/messages {}.*# #g" /etc/logrotate.conf

COPY root /

ENTRYPOINT [ "/init" ]